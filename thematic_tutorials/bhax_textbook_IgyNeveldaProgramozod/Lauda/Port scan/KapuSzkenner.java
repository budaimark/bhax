import java.net.Socket;
import java.io.IOException;

public class KapuSzkenner {
    
    public static void main(String[] args) {
    	
    	
    	if(args.length < 1)
    	{
    	System.err.println("Használat: java KapuSzkenner <ip>");
    	System.exit(-1);
    	}

        for(int i=0; i<1024; ++i)
            
            try {
                
                java.net.Socket socket = new java.net.Socket(args[0], i);
                
                System.out.println(i + " Figyeli");
                
                socket.close();
                
            } catch (Exception e) {
                
                System.out.println(i + " Nem figyeli");
                
            }
    }
    
}