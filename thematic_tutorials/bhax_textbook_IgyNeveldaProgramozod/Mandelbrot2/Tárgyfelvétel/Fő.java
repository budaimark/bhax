public class Fő {

	public static void main(String[] args) {
		
		
		Tanár tanár1 = new Tanár();
		tanár1.setName("Alan Turing");
		
		Tárgyak tárgy = new Tárgyak();
		tárgy.setName("Magas szintű programozási nyelvek");
		tárgy.setTeacher(tanár1.getName());
		tárgy.setType("Köt");
		tárgy.setCoursecode("INBPM0316-17");
		tárgy.setPersonnum(15);
		tárgy.setDay(3);
		tárgy.setBegin(10);
		tárgy.setEnd(14);
		tárgy.setMin(0);
		
		Hallgatók hallgató1 = new Hallgatók();
		hallgató1.setName("Rézműves Ágnes");
		hallgató1.setNeptuncode("GF69O"); 
		
		Hallgatók hallgató2 = new Hallgatók();
		hallgató2.setName("Horváth Géza");
		hallgató2.setNeptuncode("HQ92T");
		
		tárgy.adatok(hallgató1.getName(), hallgató1.getNeptuncode());
		tárgy.adatok(hallgató2.getName(), hallgató2.getNeptuncode());
		
		tárgy.kurzuskiir();
		tárgy.nevkiir();
	
		
		
		
				

	}

}
