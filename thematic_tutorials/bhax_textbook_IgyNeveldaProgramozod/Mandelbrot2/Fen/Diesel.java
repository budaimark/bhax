
public class Diesel extends Car {

		private String enginetype;
		private String co2emission;
		private int dconsumption;
		
		public String getEnginetype() {
			return enginetype;
		}
		public void setEnginetype(String enginetype) {
			this.enginetype = enginetype;
		}
		public String getCo2emission() {
			return co2emission;
		}
		public void setCo2emission(String co2emission) {
			this.co2emission = co2emission;
		}
		public int getDconsumption() {
			return dconsumption;
		}
		public void setDconsumption(int dconsumption) {
			this.dconsumption = dconsumption;
		}
		
		public void kiird() {
			
			System.out.println("A magasság: "+ getHeight()+ " cm");
			System.out.println("A súly: "+ getWeight()+" kg");
			System.out.println("Az ajtók száma: "+getNumberofdoors());
			System.out.println("A befogadóképesség: "+getCapacity());
			System.out.println("A márka: "+getBrand());
			System.out.println("A lóerő: "+getHorsepower()+ " HP");
			System.out.println("A Co2 kibocsátás: "+ getCo2emission());
			System.out.println("Max sebesség: "+getMaxspeed()+ " km/h");
			System.out.println("Üzemanyag fogyasztás: "+getDconsumption()+ " l/100km");
			System.out.println("Motor típus: "+getEnginetype());
			System.out.println("Származás: "+getOrigin());
		}
}
