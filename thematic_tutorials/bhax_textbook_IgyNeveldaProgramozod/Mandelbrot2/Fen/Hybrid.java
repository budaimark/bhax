
public class Hybrid extends Car  {
	
	private int numberofengines;
	private String eenginetype;
	private String denginetype;
	private String co2emission;
	
	public int getNumberofengines() {
		return numberofengines;
	}
	public void setNumberofengines(int numberofengines) {
		this.numberofengines = numberofengines;
	}
	public String getEenginetype() {
		return eenginetype;
	}
	public void setEenginetype(String eenginetype) {
		this.eenginetype = eenginetype;
	}
	public String getDenginetype() {
		return denginetype;
	}
	public void setDenginetype(String denginetype) {
		this.denginetype = denginetype;
	}
	public String getCo2emission() {
		return co2emission;
	}
	public void setCo2emission(String co2emission) {
		this.co2emission = co2emission;
	}
	public void kiirh() {
		
		System.out.println("A magasság: "+ getHeight()+ " cm");
		System.out.println("A súly: "+getWeight()+" kg");
		System.out.println("Az ajtók száma: "+getNumberofdoors());
		System.out.println("A befogadóképesség: "+getCapacity());
		System.out.println("A márka: "+getBrand());
		System.out.println("A lóerő: "+getHorsepower()+ " HP");
		System.out.println("A Co2 kibocsátás: "+getCo2emission());
		System.out.println("Max sebesség: "+getMaxspeed()+ " km/h");
		System.out.println("Dízel motor típus: "+getDenginetype());
		System.out.println("Elektromos motor típus: "+getEenginetype());
		System.out.println("Származás: "+getOrigin());
	}
}
