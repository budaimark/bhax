import java.awt.*;
import java.awt.image.BufferStrategy;

public class Labda {
	
	int startX, x = 200;
	int startY, y = 200;
	int ynov=1, xnov=1;
	int radius = 50;
	int maxX, maxY;
	private static DisplayMode[] BEST_DISPLAY_MODES = new DisplayMode[] {
			new DisplayMode(1920, 1080, 32, 0),
			new DisplayMode(1920, 1080, 16, 0),
			new DisplayMode(1920, 1080, 8, 0)
	};
		
	Frame mainFrame;
	Color ballColor, backgroundColor;
	
	public void move() {
		if ( x>=maxX-50 ) { // elerte-e a jobb oldalt?
	xnov = xnov * -1;
	startX=x;
	startY=y;
	}
	if ( x<=50 ) { // elerte-e a bal oldalt?
	xnov = xnov * -1;
	startX=x;
	startY=y;
	}
	if ( y<=50 ) { // elerte-e a tetejet?
	ynov = ynov * -1;
	startY=y;
	startX=x;
	}
	if ( y>=maxY-50 ) { // elerte-e a aljat?
	ynov = ynov * -1;
	startY=y;
	startX=x;
	}
	x = x + xnov;
	y = y + ynov;
}
public Labda (int numBuffers, GraphicsDevice device) {
	try {
	GraphicsConfiguration gc = device.getDefaultConfiguration();
	mainFrame = new Frame(gc);
	mainFrame.setUndecorated(true);
	mainFrame.setIgnoreRepaint(true);
	device.setFullScreenWindow(mainFrame);
	if (device.isDisplayChangeSupported()) {
		chooseBestDisplayMode(device);
	}
	Rectangle bounds = mainFrame.getBounds();
	bounds.setSize(device.getDisplayMode().getWidth(), device.getDisplayMode().getHeight());
	maxX = device.getDisplayMode().getWidth();
	maxY = device.getDisplayMode().getHeight();
	mainFrame.createBufferStrategy(numBuffers);
	BufferStrategy bufferStrategy = mainFrame.getBufferStrategy();
	ballColor=new Color(14,167,255);
	backgroundColor=new Color(59,65,68);
	
while(true) {
	Graphics g = bufferStrategy.getDrawGraphics();
	if (!bufferStrategy.contentsLost()) {
	move();
	g.setColor(backgroundColor);
	g.fillRect(0,0,bounds.width, bounds.height);
	g.setColor(ballColor);
	g.fillOval(x, y, radius, radius);
	bufferStrategy.show();
	g.dispose();
	}
	try {
		Thread.sleep((long) 0.5);
		} 
	catch (InterruptedException e) {}
}
	} 
	catch (Exception e) {
	e.printStackTrace();
	} 
	finally {
	device.setFullScreenWindow(null);
	}
	}
private static DisplayMode getBestDisplayMode(GraphicsDevice device) {
	for (int x = 0; x < BEST_DISPLAY_MODES.length; x++) {
		DisplayMode[] modes = device.getDisplayModes();
		for (int i = 0; i < modes.length; i++) {
		if (modes[i].getWidth() == BEST_DISPLAY_MODES[x].getWidth()
		&& modes[i].getHeight() == BEST_DISPLAY_MODES[x].getHeight()
		&& modes[i].getBitDepth() == BEST_DISPLAY_MODES[x].getBitDepth()) 
		{
		return BEST_DISPLAY_MODES[x];
		}
		}
		}
		return null;
		}
	public static void chooseBestDisplayMode(GraphicsDevice device) {
		DisplayMode best = getBestDisplayMode(device);
		if (best != null) {
		device.setDisplayMode(best);
		}
	}





public static void main(String[] args) {
	try {
		int numBuffers = 2;
		GraphicsEnvironment env = GraphicsEnvironment.
		getLocalGraphicsEnvironment();
		GraphicsDevice device = env.getDefaultScreenDevice();
		Labda ball = new Labda(numBuffers, device);
		} catch (Exception e) {
		e.printStackTrace();
		}
		System.exit(0);
		}

}


