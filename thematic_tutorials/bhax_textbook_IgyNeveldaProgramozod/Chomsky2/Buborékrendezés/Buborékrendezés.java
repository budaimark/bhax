import java.util.ArrayList;

public class Buborékrendezés {
	
	public static ArrayList<Integer> rendez(ArrayList<Integer> számok) {
		
		for(int i = (számok.size() -1); i>0; i--) {
			for (int j = 0; j<i; j++) {
				
				if (számok.get(j)> számok.get(j+1)) {
					int temp = számok.get(j);
					számok.set(j, számok.get(j+1));
					számok.set((j+1), temp);	
				}
			}
		}
		
		return számok;
	}
	
}
