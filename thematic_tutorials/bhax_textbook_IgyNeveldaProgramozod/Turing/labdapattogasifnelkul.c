
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <curses.h>

int main()
{
	int xi=0, xj=0, yi=0, yj=0;
	int mx=80*2, my=24*2;

	WINDOW *ablak;
	ablak = initscr();

	nodelay(ablak, true);
	noecho ();
	cbreak ();

	while(true)
	{
		xi= (xi-1) % mx;
		xj= (xj+1) % mx;

		yi= (yi-1) % my;
		yj= (yj+1) % my;

		//clear ();

        mvprintw (0,0,  "---------------------------------------------------------------------------------");
        mvprintw (24,0, "---------------------------------------------------------------------------------");

		mvprintw (abs((yi+(my-yj))/2),abs((xi+(mx-xj))/2),"O");

		refresh();
		usleep (50000);
	}
	return 0;
}