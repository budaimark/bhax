import java.io.InputStream;
import java.io.OutputStream;

public class xor {
    
    public xor(String kulcs_szöveg,
            java.io.InputStream bejövő_csatorna,
            java.io.OutputStream kimenő_csatorna)
            throws java.io.IOException {
        
        byte [] kulcs = kulcs_szöveg.getBytes();
        byte [] buffer = new byte[256];
        int kulcs_index = 0;
        int olvasott_bájtok = 0;
        while((olvasott_bájtok = bejövő_csatorna.read(buffer)) != -1) 
        {
            
            for(int i=0; i<olvasott_bájtok; ++i) {
                
                buffer[i] = (byte)(buffer[i] ^ kulcs[kulcs_index]);
                kulcs_index = (kulcs_index+1) % kulcs.length;
                
            }
            
            kimenő_csatorna.write(buffer, 0, olvasott_bájtok);
            
        }
        
    }
    
    public static void main(String[] args) {
        
        try {
            
            new xor(args[0], System.in, System.out);
            
        } catch(java.io.IOException e) {
            
            e.printStackTrace();
            
        }
        
    }
    
}