#include<iostream>

int* atombomba_mutato(int* atombomba_tomb);
typedef int(*R)(int,int);
int hiroshima_allapot(int a, int b);
R hiroshima_allapot_ma(int);


int main() {
//egész 
int hiroshima = 1945;
//egészre mutató mutató 
int *ToHiroshima = &hiroshima;
//C++  
//int &refToHiroshima = hiroshima;
//egészek tömbje 
int atombomba_tipusok[4] = {1,9,4,5};
//egészek tömbjének referenciája 
int (&refToAtombomba)[4]= atombomba_tipusok;
//egészre mutató mutatók tömbje 
int * atombomba_array[4];
//egészre mutató mutatót visszaadó függvény 
int* myAtombomba = atombomba_mutato(atombomba_tipusok);
//egészre mutató mutatót visszaadó függvényre mutató mutató
int* (*pointerFUN)(int*) = atombomba_mutato;
//egészet visszaadó és két egészet kapó függvényre mutató mutatót visszaadó, egészet kapó függvény 
R hiroshima_allapot = hiroshima_allapot_ma(1986);
return 0;
} 
int* atombomba_mutato(int* atombomba_tomb) { 
    return atombomba_tomb; 
} 
int hiroshima_allapot(int a, int b) {
    return -1945; 
}
R hiroshima_allapot_ma(int wishGranter) {
    return hiroshima_allapot; 
}
