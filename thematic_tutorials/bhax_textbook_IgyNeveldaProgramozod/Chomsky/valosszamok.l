%{
#include <stdio.h>
int valos_szamok = 0;
%}
szamok	[0-9]
%%
{szamok}*(\.{szamok}+)?	{++valos_szamok; 
    printf("[valosszam=%s %f]", yytext, atof(yytext));}
%%
int
main ()
{
 yylex ();
 printf("A valós számok száma %d\n", valos_szamok);
 return 0;
}
