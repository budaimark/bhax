

#include <iostream>
using namespace std;

class Madar {
public:
    virtual void repul() {}
};

class Program {
public:
    void fuggveny(Madar& madar) {
        madar.repul();
    }

};

class Solyom : public Madar{};

class Pingvin : public Madar {};




int main()
{
    Program progi;
    Madar madar;
    progi.fuggveny(madar);
    Solyom solyom;
    progi.fuggveny(solyom);
    Pingvin pingvin;
    progi.fuggveny(pingvin);

}

