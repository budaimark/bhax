#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int main()
{
	vector<int> szamok= {5, -1, 2, -7, 4};
	cout << "Rendezés előtt:\n";
	for(auto& i : szamok){
		cout << i << " ";
	}
	cout << endl;
		sort(szamok.begin(), szamok.end(), [](int a, int b){ return a < b;});
	cout << "Rendezés után:\n";
	for(auto& i : szamok){
		cout << i << " ";
	}
	cout << endl;
}
