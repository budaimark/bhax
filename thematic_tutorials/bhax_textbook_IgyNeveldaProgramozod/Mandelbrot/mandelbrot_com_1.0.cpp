#include <iostream>
#include <png++/png.hpp>
#include <sys/times.h>
#include <complex>

#define SIZE 600
#define ITERATION_LIMIT 32000
using namespace std;

void mandel (int buffer[SIZE][SIZE]) {
    clock_t delta = clock ();
    struct tms tmsbuf1, tmsbuf2;
    times (&tmsbuf1);

    float a = -2.0, b = .7, c = -1.35, d = 1.35;
    int width = SIZE, height = SIZE, iterationLimit = ITERATION_LIMIT;

    // a számítás
    float dx = (b - a) / width;
    float dy = (d - c) / height;
    float reC, imC;

    int iteration = 0;

    for (int j = 0; j < height; ++j)
    {
        //sor = j;
        for (int k = 0; k < width; ++k)
        {
            reC = a + k * dx;
            imC = d - j * dy;
            complex<double> complexNum(reC,imC);
            complex<double> complexHelper(0,0);
            iteration = 0;
            while (std::abs(complexHelper) < 4 && iteration <
            iterationLimit)
            {
                complexHelper = complexHelper*complexHelper + complexNum;
                ++iteration;
            }
            buffer[j][k] = iteration;
        }
    }
    times (&tmsbuf2);
    cout << tmsbuf2.tms_utime - tmsbuf1.tms_utime
            + tmsbuf2.tms_stime - tmsbuf1.tms_stime << "\n";
    delta = clock () - delta;
    cout << (float) delta / CLOCKS_PER_SEC << " sec\n";
}

int main (int argc, char *argv[])
{
    if (argc != 2)
    {
        std::cout << "Hasznalat: ./mandelpng fajlnev\n";
        return -1;
    }
    png::image < png::rgb_pixel > image(SIZE,SIZE);
    int buffer[SIZE][SIZE];
    mandel(buffer);
    for (int j = 0; j < SIZE; ++j)
    {
        //sor = j;
        for (int k = 0; k < SIZE; ++k)
        {
            image.set_pixel(k,j,png::rgb_pixel((255 - (255 * buffer[j][k]) / ITERATION_LIMIT) ,
            (255 - (255 * buffer[j][k]) / ITERATION_LIMIT),
            (255 - (255 * buffer[j][k]) / ITERATION_LIMIT)));
        }
    }
    image.write(argv[1]);
    cout << argv[1] <<" mentve\n";
}
