
public class MandelbrotHalmaz extends java.awt.Frame implements Runnable {
        protected double a, b, c, d;

        protected int szélesség, magasság;

        protected java.awt.image.BufferedImage kép;

        protected int iterációsHatár = 255;

        protected boolean számításFut = false;

        protected int sor = 0;

        protected static int pillanatfelvételSzámláló = 0;

    public MandelbrotHalmaz(final double a, final double b, final double c, final double d, final int szélesség,
            final int iterációsHatár) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.szélesség = szélesség;
        this.iterációsHatár = iterációsHatár;
        this.magasság = (int) (szélesség * ((d - c) / (b - a)));

        kép = new java.awt.image.BufferedImage(szélesség, magasság, java.awt.image.BufferedImage.TYPE_INT_RGB);

        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(final java.awt.event.WindowEvent e) {
                setVisible(false);
                System.exit(0);
            }
        });
        addKeyListener(new java.awt.event.KeyAdapter() {

            public void keyPressed(final java.awt.event.KeyEvent e) {
                if (e.getKeyCode() == java.awt.event.KeyEvent.VK_S)
                    pillanatfelvétel();

                else if (e.getKeyCode() == java.awt.event.KeyEvent.VK_N) {
                    if (számításFut == false) {
                        MandelbrotHalmaz.this.iterációsHatár += 256;
                        // A számítás újra indul:
                        számításFut = true;
                        new Thread(MandelbrotHalmaz.this).start();
                    }
                } else if (e.getKeyCode() == java.awt.event.KeyEvent.VK_M) {
                    if (számításFut == false) {
                        MandelbrotHalmaz.this.iterációsHatár += 10 * 256;
                        // A számítás újra indul:
                        számításFut = true;
                        new Thread(MandelbrotHalmaz.this).start();
                    }
                }
            }
        });
        // Ablak tulajdonságai
        setTitle("A Mandelbrot halmaz");
        setResizable(false);
        setSize(szélesség, magasság);
        setVisible(true);
        // A számítás indul:
        számításFut = true;
        new Thread(this).start();
    }

    public void paint(final java.awt.Graphics g) {
        // A Mandelbrot halmaz kirajzolása
        g.drawImage(kép, 0, 0, this);
        // Ha éppen fut a számítás, akkor egy vörös
        // vonallal jelöljük, hogy melyik sorban tart:
        if (számításFut) {
            g.setColor(java.awt.Color.RED);
            g.drawLine(0, sor, getWidth(), sor);
        }
    }

    public void update(final java.awt.Graphics g) {
        paint(g);
    }

    public void pillanatfelvétel() {

        final java.awt.image.BufferedImage mentKép = new java.awt.image.BufferedImage(szélesség, magasság,
                java.awt.image.BufferedImage.TYPE_INT_RGB);
        final java.awt.Graphics g = mentKép.getGraphics();
        g.drawImage(kép, 0, 0, this);
        g.setColor(java.awt.Color.BLUE);
        g.drawString("a=" + a, 10, 15);
        g.drawString("b=" + b, 10, 30);
        g.drawString("c=" + c, 10, 45);
        g.drawString("d=" + d, 10, 60);
        g.drawString("n=" + iterációsHatár, 10, 75);
        g.dispose();

        StringBuffer sb = new StringBuffer();
        sb = sb.delete(0, sb.length());
        sb.append("MandelbrotHalmaz_");
        sb.append(++pillanatfelvételSzámláló);
        sb.append("_");

        sb.append(a);
        sb.append("_");
        sb.append(b);
        sb.append("_");
        sb.append(c);
        sb.append("_");
        sb.append(d);
        sb.append(".png");

        try {
            javax.imageio.ImageIO.write(mentKép, "png", new java.io.File(sb.toString()));
        } catch (final java.io.IOException e) {
            e.printStackTrace();
        }
    }

    public void run() {
        final double dx = (b - a) / szélesség;
        final double dy = (d - c) / magasság;
        double reC, imC, reZ, imZ, ujreZ, ujimZ;
        int rgb;

        int iteráció = 0;

        for (int j = 0; j < magasság; ++j) {
            sor = j;
            for (int k = 0; k < szélesség; ++k) {
                reC = a + k * dx;
                imC = d - j * dy;

                reZ = 0;
                imZ = 0;
                iteráció = 0;

                while (reZ * reZ + imZ * imZ < 4 && iteráció < iterációsHatár) {
                    // z_{n+1} = z_n * z_n + c
                    ujreZ = reZ * reZ - imZ * imZ + reC;
                    ujimZ = 2 * reZ * imZ + imC;
                    reZ = ujreZ;
                    imZ = ujimZ;

                    ++iteráció;
                }
                rgb = (255 - iteráció) | ((255 - iteráció) << 8) | ((255 - iteráció) << 16);
                kép.setRGB(k, j, rgb);
            }
            repaint();
        }
        számításFut = false;
    }

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    public double getC() {
        return c;
    }

    public double getD() {
        return d;
    }

    public int getSz() {
        return szélesség;
    }

    public int getM() {
        return magasság;
    }

    public java.awt.image.BufferedImage kép() {
        return kép;
    }

    public static void main(final String[] args) {
        // A halmazt a komplex sík [-2.0, .7]x[-1.35, 1.35] tartományában
        // keressük egy 400x400-as hálóval:
        new MandelbrotHalmaz(-2.0, .7, -1.35, 1.35, 600, 255);
    }
}



